""
" Name:         autobot
" Description:  A bold, transformative colour-scheme!
" Website:      https://github.com/mookle/autobot-vim.git
" Author:       Mookle
""
let g:colors_name = "autobot"

hi clear
if exists("syntax_on")
    syntax reset
endif

set background=dark
set t_Co=256

" 0   black
" 31  blue-grey
" 45  electric blue
" 160 red
" 220 yellow
" 230 off-white
" 233 almost black
" 236 grey
" 241 light grey

hi Normal       guifg=#ffffd7   guibg=#121212   gui=NONE    ctermfg=230 ctermbg=233 cterm=NONE
hi Function     guifg=#00d7ff   guibg=#121212   gui=BOLD    ctermfg=45  ctermbg=233 cterm=BOLD
hi Special      guifg=#d70000   guibg=#121212   gui=NONE    ctermfg=160 ctermbg=233 cterm=NONE
hi Constant     guifg=#ffd700   guibg=#121212   gui=NONE    ctermfg=220 ctermbg=233 cterm=NONE
hi Comment      guifg=#626262   guibg=#121212   gui=NONE    ctermfg=241 ctermbg=233 cterm=NONE
hi Statement    guifg=#0087af   guibg=#121212   gui=NONE    ctermfg=31  ctermbg=233 cterm=NONE
hi Cursor       guifg=#121212   guibg=#e4e4e4   gui=NONE    ctermfg=233	ctermbg=254 cterm=NONE
hi CursorLine  	                guibg=#444444	gui=NONE                ctermbg=238 cterm=NONE
hi LineNr       guifg=#444444   guibg=#000000   gui=NONE    ctermfg=238 ctermbg=0   cterm=NONE
hi StatusLine   guifg=#000000   guibg=#ffffd7   gui=NONE    ctermfg=0   ctermbg=230 cterm=NONE
hi StatusLineNC guifg=#000000   guibg=#444444   gui=NONE    ctermfg=0   ctermbg=238 cterm=NONE
hi Search       guifg=#d70000   guibg=#ffffd7   gui=NONE    ctermfg=160 ctermbg=230 cterm=NONE
hi IncSearch    guifg=#000000   guibg=#ffffd7   gui=NONE    ctermfg=0   ctermbg=230 cterm=NONE
hi Error        guifg=#ffffd7   guibg=#d70000   gui=NONE    ctermfg=230 ctermbg=160 cterm=NONE
hi ExtraWhitespace              guibg=#d70000                           ctermbg=160

hi! link    Operator    Special
hi! link    Number      Constant
hi! link    String      Constant
hi! link    Type        Statement
hi! link    PreProc     Statement
hi! link    SignColumn  LineNr
hi! link    Directory   Statement
hi! link    Title       Special

" PHP specific
hi phpVarSelector    guifg=#d7d7af  guibg=#121212               ctermfg=187 ctermbg=233
hi phpMemberSelector guifg=#005f87  guibg=#121212   gui=NONE    ctermfg=24  ctermbg=233 cterm=NONE

hi! link    phpIdentifier       Normal
hi! link    phpFunction         Function
hi! link    phpClasses          Function
hi! link    phpOperator         Special
hi! link    phpKeyword          Special
hi! link    phpType             Special
hi! link    phpRegion           Statement
hi! link    phpStaticClasses    Statement
hi! link    phpMethodsVar       Statement
hi! link    phpParent           Statement

" YAML specific
hi! link yamlKey    Statement

" Javascript specific
hi! link javaScriptStringD      Normal

