## Description
Inspired by the old-school Autobot colours, and designed for use with a 256-colour terminal.

## Screenshots
### PHP / YAML
![autobot screenshot](screenshot.png)

## Further development
This theme isn't intended to be terribly robust; it's only really used by me for PHP development. If you enjoy working
with it, please consider submitting a pull request to help improve the number of supported languages.
